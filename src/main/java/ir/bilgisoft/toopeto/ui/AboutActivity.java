package ir.bilgisoft.toopeto.ui;

import android.app.Activity;
import android.os.Bundle;

import ir.bilgisoft.toopeto.R;

public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
