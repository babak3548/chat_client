package ir.bilgisoft.toopeto.xmpp;

public interface OnMessageAcknowledged {
	public void onMessageAcknowledged(ir.bilgisoft.toopeto.entities.Account account, String id);
}
