package ir.bilgisoft.toopeto.xmpp;

public interface OnStatusChanged {
	public void onStatusChanged(ir.bilgisoft.toopeto.entities.Account account);
}
