package ir.bilgisoft.toopeto.xmpp;

/**
 * Created by mehrang on 12/02/2015.
 */
public interface OnScrollRooms {
    public  void onScrollRooms(String lastItem, int totalItemCountListView);
}
