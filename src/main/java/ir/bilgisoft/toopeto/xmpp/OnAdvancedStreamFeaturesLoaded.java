package ir.bilgisoft.toopeto.xmpp;

import ir.bilgisoft.toopeto.entities.Account;

public interface OnAdvancedStreamFeaturesLoaded {
	public void onAdvancedStreamFeaturesAvailable(final Account account);
}
