package ir.bilgisoft.toopeto.xmpp;

public interface OnBindListener {
	public void onBind(ir.bilgisoft.toopeto.entities.Account account);
}
