package ir.bilgisoft.toopeto.xmpp.jingle;

public interface OnTransportConnected {
	public void failed();

	public void established();
}
