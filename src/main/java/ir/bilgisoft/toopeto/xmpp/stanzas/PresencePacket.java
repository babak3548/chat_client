package ir.bilgisoft.toopeto.xmpp.stanzas;

public class PresencePacket extends AbstractStanza {

	public PresencePacket() {
		super("presence");
	}
}
